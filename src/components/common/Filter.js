import React from 'react';

import Checkbox from './Checkbox';

function Filter(props) {
	const handleCheckboxChange = (checkboxID, checked) => {
		props.handleChange(checkboxID, props.id, checked)
	};

	return (
		<React.Fragment>
			{props.filters.map((filter, index) => {
				return (
					<Checkbox id={index} checkbox={filter} handleChange={handleCheckboxChange} />
				)
			})}
		</React.Fragment>
	);
}

export default Filter;
