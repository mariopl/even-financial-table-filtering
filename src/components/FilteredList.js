import React, { useState, useEffect } from 'react';
import CssBaseline from '@material-ui/core/CssBaseline';
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';

import './FilteredList.css';
import Filters from './common/Filters';

import itineraries from './data/itineraries.json';

function FilteredList() {
	const [data, setData] = useState([])
	const [filters, setFilters] = useState([])
	const [filterItems, setFilterItems] = useState([])

	// I use this array so I can choose by which fields the table can be filtered
	const activeFilters = ['airline', 'stops']

	const getData = () => {
		const data = JSON.parse(JSON.stringify(itineraries));

		// going through just the first array object in order to get the filter values ('airline', 'stops', 'cost')
		Object.keys(data[0]).map((key, i) => {
			// checking if current value exists in the "activeFilters" array
			if(activeFilters.indexOf(key) !== -1) {
				let uniqueItems = []

				// going through all of the array values in order to get the unique values for filters
				data.map((item) => {
					// checking if current value is a duplicate
					let duplicateIndex = uniqueItems.findIndex(i => i.value === item[key])

					if(duplicateIndex === -1) {
						// if the value doesn't exist yet push it into the array
						uniqueItems.push({ 'value': item[key], 'isChecked': true, 'count': 1 })
					} else {
						// if it's a duplicate then increase count
						uniqueItems[duplicateIndex]['count']++
					}
				})

				// sort filter items alphabetically/numerically
				uniqueItems.sort(function(a, b) {
					return a.value - b.value
				})

				setFilters(filters => [...filters, key])
				setFilterItems(filterItems => [...filterItems, uniqueItems])
			}
		})

		setData(data)
	}

	useEffect(() => {
		getData()
	}, [])

	const handleChange = (checkboxID, filterID, checked) => {
		let originalData = JSON.parse(JSON.stringify(itineraries));
		let newFilterItems = filterItems
		let filteredData = []

		// set new checkbox value to filter items
		newFilterItems[filterID][checkboxID]['isChecked'] = checked
		// reset all filter items count to 0
		newFilterItems.map((filter) => {
			filter.map((item) => {
				item['count'] = 0
			})
		})

		// iterate through all currently active filters
		activeFilters.map((filter, filterIndex) => {
			// iterate through all items
			// in every iteration some items get left out because their filter is not checked
			originalData.map((item, itemIndex) => {
				// get filter value
				const filterValue = newFilterItems[filterIndex].find(i => i.value === item[filter]);

				// check if current filter is checked
				if(filterValue['isChecked']) {
					// if filter is checked add item to temporary "filteredData" array
					filteredData.push(item)
				}

				// assign filteredData value to original data so that in next iteration
				// we don't unnecessarily go through items that were already filtered out
				originalData = filteredData
			})

			// after going through all of the items reset temporary field for next active filter iteration
			// otherwise items would get duplicated
			filteredData = []
		})

		activeFilters.map((filter, filterIndex) => {
			originalData.map((item, itemIndex) => {
				const currentFilterIndex = newFilterItems[filterIndex].findIndex(i => i.value === item[filter])

				filteredData.push(item)
				newFilterItems[filterIndex][currentFilterIndex]['count']++
			})
		})

		// set new filter items and data
		setFilterItems(newFilterItems)
		setData(originalData)
	};

	return (
		<React.Fragment>
			<CssBaseline />
			<Container maxWidth="lg">
				<Grid container spacing={3}>
					<Grid item xs={12} sm={4} md={3}>
						<h2>Filters</h2>

						<Filters filters={filters} filterItems={filterItems} handleChange={handleChange} />
					</Grid>
					<Grid item xs={12} sm={8} md={9}>
						<h2>Results</h2>

						<Table size="small">
							<TableHead>
								<TableRow>
									<TableCell>ID</TableCell>
									<TableCell>Airline</TableCell>
									<TableCell>Stops</TableCell>
									<TableCell>Fare</TableCell>
								</TableRow>
							</TableHead>
							<TableBody>
							{data.map((item) => (
								<TableRow key={item.id}>
									<TableCell>{item.id}</TableCell>
									<TableCell>{item.airline}</TableCell>
									<TableCell>{item.stops}</TableCell>
									<TableCell>${item.cost}</TableCell>
								</TableRow>
							))}
							</TableBody>
						</Table>
					</Grid>
				</Grid>
			</Container>
		</React.Fragment>
	);
}

export default FilteredList;
