import React, { useState } from 'react';
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import MaterialCheckbox from '@material-ui/core/Checkbox';

function Checkbox(props) {
	const [checked, setChecked] = useState(true)

	const handleCheckboxChange = (event) => {
		setChecked(event.target.checked)
		props.handleChange(props.id, event.target.checked)
	};

	return (
		<React.Fragment>
			<FormGroup>
				<FormControlLabel
					control={<MaterialCheckbox checked={checked} onChange={handleCheckboxChange} name="test" />}
					label={props.checkbox.value + ' (' + props.checkbox.count + ')'}
				/>
			</FormGroup>
		</React.Fragment>
	);
}

export default Checkbox;
