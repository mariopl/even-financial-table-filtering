import React from 'react';

import Filter from './Filter';

function Filters(props) {
	const handleCheckboxChange = (checkboxID, filterID, checked) => {
		props.handleChange(checkboxID, filterID, checked)
	};

	return (
		<div class="Filters">
			{props.filters.map((filter, index) => (
				<div>
					<h3 class="Filters-title">{filter}</h3>
					<Filter id={index} filters={props.filterItems[index]} handleChange={handleCheckboxChange} />
				</div>
			))}
		</div>
	);
}

export default Filters;
